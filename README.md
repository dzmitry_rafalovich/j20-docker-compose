# Docker compose project for J20 Final Project

## Useful commands:
- `docker-compose up --detach` - run all containers from the docker-compose.yml
- `docker-compose up --build --detach` - rebuild and run all containers from the docker-compose.yml
- `docker stop $(docker ps -q)` - stop all running containers
- `docker rm $(docker ps -a -q)` - remove all containers
- `docker-compose down` - stop and remove all containers
- `docker-compose down --volumes` - stop and remove all containers + associated volumes !!!!!!!!! (drop all database data) !!!!!!

## Useful tips:
1. To run containers on Mac and Windows devices change docker ip value on:
   `DOCKER_IP=host.docker.internal`

create user j20_cafe with password 'j20_cafe';
create database j20_cafe with owner j20_cafe;

create user j20_cinema with password 'j20_cinema';
create database j20_cinema with owner j20_cinema;

create user j20_garage with password 'j20_garage';
create database j20_garage with owner j20_garage;

create user j20_static with password 'j20_static';
create database j20_static with owner j20_static;

create user j20_ticket with password 'j20_ticket';
create database j20_ticket with owner j20_ticket;

create user j20_user with password 'j20_user';
create database j20_user with owner j20_user;